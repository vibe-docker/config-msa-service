FROM anapsix/alpine-java
LABEL maintainer="devops@vibedesenv.com"
COPY target/config-msa-service-0.0.1-SNAPSHOT.jar /home/config-msa-service-0.0.1-SNAPSHOT.jar
CMD ["java","-Xmx128m","-jar", "-Dspring.profiles.active=cloud","/home/config-msa-service-0.0.1-SNAPSHOT.jar"]